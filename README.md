# helm-values-linter

Валидация Values-файлов, которая строится на проверке соответствия схеме, описанной в файле `schema-all-v1.yaml`.

## Использование

Достаточно выполнить команду:

```shell
% helm-values-linter -f helm/prod.values.yaml
INFO[0000] Looks good
```

При этом из каталога `helm/` будет дополнительно прочитан файл `values.yaml`, который, согласно текущей схеме публикации, содержит общее описание приложения для всех окружений.

По аналогии с Helm, можно дополнительно устанавливать параметры через аргументы, убедитесь, что они допустимы:

```shell
% helm-values-linter -f helm/prod.values.yaml --set=foo=bar
ERRO[0000] (root): Additional property foo is not allowed
```

Файлы вида `foobar.values.yaml` можно дополнительно проверять по специализированной схеме, помимо общей, для этого нужно создать файл `schema-foobar-v1.yaml` с нужным описанием.

## Подключение

В `.gitlab-ci.yml` добавить шаг:

```yaml
helm:
  image: eu.gcr.io/qlean-242314/gitlab/platform/infra/helm-values-linter
  stage: testing
  # allow_failure: true
  script:
    - helm-values-linter -f helm/prod.values.yaml
```

## Добавление новой версии схемы

Рассмотрим пример добавления бета-версии новой несовместимой схемы, для этого создадим файл `schema-v2beta1.yaml` с описанием схемы. Далее пересобираем бинарный файл, чтобы включить новую схему в сборку, затем, в проверяемом файле указываем схему, относительно которой будет происходить валидация:

```yaml
---
schemaVersion: "2beta1"
```
