package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCronFormat_IsFormat(t *testing.T) {
	c := &CronFormat{}
	tests := []struct {
		input interface{}
		valid bool
	}{
		{
			input: nil,
			valid: false,
		},
		{
			input: 1,
			valid: false,
		},
		{
			input: true,
			valid: false,
		},
		{
			input: "foobar",
			valid: false,
		},
		{
			input: "*/1 * * * *",
			valid: true,
		},
	}
	for caseID, test := range tests {
		assert.Equal(t, test.valid, c.IsFormat(test.input), caseID)
	}
}
