package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDurationFormat_IsFormat(t *testing.T) {
	d := &DurationFormat{}
	assert.True(t, d.IsFormat("10m"))
	assert.False(t, d.IsFormat(true))
	assert.False(t, d.IsFormat("foobar"))
}
