package main

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMain_Main(t *testing.T) {
	exitFn = func(code int) {
		fmt.Println("exit", code)
	}
	main()
}

func Test_Run(t *testing.T) {
	_ = os.Setenv("INJECT_APP_TEMPLATE", "true")
	c := newCommand()
	assert.NotNil(t, c)
	err := c.RunE(nil, []string{"fixtures/command/valid"})
	assert.NoError(t, err)

	_ = os.Setenv("INJECT_APP_TEMPLATE", "true")
	c = newCommand()
	assert.NotNil(t, c)
	err = c.RunE(nil, []string{"fixtures/command/invalid_additional"})
	assert.Error(t, err)

	_ = os.Setenv("INJECT_APP_TEMPLATE", "false")
	c = newCommand()
	assert.NotNil(t, c)
	err = c.RunE(nil, []string{"fixtures/command/invalid_syntax"})
	assert.Error(t, err)

	_ = os.Setenv("INJECT_APP_TEMPLATE", "false")
	c = newCommand()
	assert.NotNil(t, c)
	err = c.RunE(nil, []string{"fixtures/command/invalid_schema_version"})
	assert.Error(t, err)

	o := &options{}
	o.ValueFiles = []string{
		"example/values.yaml",
	}
	err = run(nil, o)
	assert.NoError(t, err)

	o = &options{}
	o.ValueFiles = []string{
		"example/prod.values.yaml",
	}
	err = run(nil, o)
	assert.NoError(t, err)

	o = &options{}
	o.ValueFiles = []string{
		"example/foobar.values.yaml",
	}
	err = run(nil, o)
	assert.NoError(t, err)
}
