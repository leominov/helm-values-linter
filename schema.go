package main

import (
	"embed"
	_ "embed"
	"fmt"
	"path"
	"strings"

	"github.com/xeipuuv/gojsonschema"
	"sigs.k8s.io/yaml"
)

var (
	//go:embed schema-*.yaml
	schemas              embed.FS
	schemaTemplate       = "schema-%s.yaml"
	defaultSchemaVersion = "1"
)

func init() {
	gojsonschema.FormatCheckers.Add("cron", CronFormat{})
	gojsonschema.FormatCheckers.Add("duration", DurationFormat{})
}

func LoadSchema(schemaType, version string) (*gojsonschema.Schema, error) {
	version = strings.TrimPrefix(version, "v")
	version = fmt.Sprintf("%s-v%s", schemaType, version)
	schemaBytes, err := schemas.ReadFile(fmt.Sprintf(schemaTemplate, version))
	if err != nil {
		return nil, fmt.Errorf("unknown schema version: %s", version)
	}
	var bodySchema map[string]interface{}
	_ = yaml.Unmarshal(schemaBytes, &bodySchema)
	schemaLoader := gojsonschema.NewGoLoader(bodySchema)
	return gojsonschema.NewSchema(schemaLoader)
}

func ValidateWithSchema(schema *gojsonschema.Schema, vals map[string]interface{}) (*gojsonschema.Result, error) {
	loader := gojsonschema.NewGoLoader(vals)
	return schema.Validate(loader)
}

func ExtractSchemaVersion(vals map[string]interface{}) string {
	if versionRaw, ok := vals["schemaVersion"]; ok {
		if version, ok := versionRaw.(string); ok {
			return version
		}
	}
	return defaultSchemaVersion
}

func ExtractTypes(files []string) []string {
	var (
		types   = []string{"all"}
		typeMap = make(map[string]bool)
	)
	for _, f := range files {
		name := path.Base(f)
		data := strings.Split(name, ".")
		if len(data) > 2 {
			_, ok := typeMap[data[0]]
			if ok {
				continue
			}
			types = append(types, data[0])
			typeMap[data[0]] = true
		}
	}
	return types
}
