package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestWorkDir(t *testing.T) {
	assert.Equal(t, ".", WorkDir(nil))
	assert.Equal(t, ".", WorkDir([]string{}))
	assert.Equal(t, "foobar", WorkDir([]string{"foobar"}))
}

func TestExtractCommonDir(t *testing.T) {
	assert.Equal(t, "", ExtractCommonDir(nil))
	assert.Equal(t, "", ExtractCommonDir([]string{}))
	assert.Equal(t, "", ExtractCommonDir([]string{"foobar/barfoo", "barfoo/foobar"}))
	assert.Equal(t, "foobar", ExtractCommonDir([]string{"foobar/item-1", "foobar/item-1"}))
}
