package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"sigs.k8s.io/yaml"
)

func loadFixture(filename string) (map[string]interface{}, error) {
	body, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	vals := make(map[string]interface{})
	err = yaml.Unmarshal(body, &vals)
	return vals, err
}

func TestValidateWithSchema(t *testing.T) {
	_, err := LoadSchema("all", "999")
	assert.Error(t, err)
	schema, err := LoadSchema("all", "1")
	if !assert.NoError(t, err) {
		return
	}
	vals, err := loadFixture("fixtures/schema/app_template_required.yaml")
	if !assert.NoError(t, err) {
		return
	}
	result, err := ValidateWithSchema(schema, vals)
	if !assert.NoError(t, err) {
		return
	}
	if assert.Equal(t, 1, len(result.Errors())) {
		resultErr := result.Errors()[0]
		assert.Contains(t, resultErr.String(), "app-template is required")
	}
	vals, err = loadFixture("fixtures/schema/root_additional_properties.yaml")
	if !assert.NoError(t, err) {
		return
	}
	result, err = ValidateWithSchema(schema, vals)
	if !assert.NoError(t, err) {
		return
	}
	if assert.Equal(t, 2, len(result.Errors())) {
		resultErr := result.Errors()[0]
		assert.Contains(t, resultErr.String(), "app-template is required")
		resultErr = result.Errors()[1]
		assert.Contains(t, resultErr.String(), "property foobar is not allowed")
	}
	vals, err = loadFixture("fixtures/schema/full_valid.yaml")
	if !assert.NoError(t, err) {
		return
	}
	result, err = ValidateWithSchema(schema, vals)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, 0, len(result.Errors()))
	vals, err = loadFixture("fixtures/schema/app_template_app_incorrect_format.yaml")
	if !assert.NoError(t, err) {
		return
	}
	result, err = ValidateWithSchema(schema, vals)
	if !assert.NoError(t, err) {
		return
	}
	if assert.Equal(t, 2, len(result.Errors())) {
		resultErr := result.Errors()[0]
		assert.Contains(t, resultErr.String(), "Invalid type")
		resultErr = result.Errors()[1]
		assert.Contains(t, resultErr.String(), "Invalid type")
	}
}

func TestExtractSchemaVersion(t *testing.T) {
	vals, err := loadFixture("fixtures/schema/schema_version_default.yaml")
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, "1", ExtractSchemaVersion(vals))
	vals, err = loadFixture("fixtures/schema/schema_version_specified.yaml")
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, "2", ExtractSchemaVersion(vals))
	assert.Equal(t, "1", ExtractSchemaVersion(map[string]interface{}{
		"schemaVersion": 2,
	}))
}

func TestExtractTypes(t *testing.T) {
	assert.Equal(t, []string{"all"}, ExtractTypes([]string{"values.yaml"}))
	assert.Equal(t, []string{"all"}, ExtractTypes([]string{"foobar.yaml"}))
	types := ExtractTypes([]string{"values.yaml", "b.values.yaml", "a.values.yaml", "a.values.yaml"})
	if assert.Equal(t, 3, len(types)) {
		assert.Equal(t, "all", types[0])
	}
}
