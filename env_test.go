package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEnvBoolOrDef(t *testing.T) {
	os.Unsetenv("FOOBAR")
	assert.True(t, EnvBoolOrDef("FOOBAR", true))
	assert.False(t, EnvBoolOrDef("FOOBAR", false))

	os.Setenv("FOOBAR", "1")
	assert.True(t, EnvBoolOrDef("FOOBAR", true))
	assert.True(t, EnvBoolOrDef("FOOBAR", false))

	os.Setenv("FOOBAR", "0")
	assert.False(t, EnvBoolOrDef("FOOBAR", true))
	assert.False(t, EnvBoolOrDef("FOOBAR", false))
}
