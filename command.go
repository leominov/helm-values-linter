package main

import (
	"errors"
	"os"
	"path"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"helm.sh/helm/v3/pkg/cli/values"
)

var (
	commonUsedValueFiles = []string{
		"preprod.values.yaml",
		"prod.values.yaml",
		"stage.values.yaml",
	}
)

type options struct {
	values.Options
	injectAppTemplate bool
	lintAllValueFiles bool
}

func newCommand() *cobra.Command {
	opts := &options{}
	cmd := &cobra.Command{
		Use:          "helm-values-linter",
		SilenceUsage: true,
		RunE: func(_ *cobra.Command, args []string) error {
			if opts.lintAllValueFiles {
				return runAll(args, opts)
			}
			return run(args, opts)
		},
	}

	flags := cmd.PersistentFlags()
	flags.BoolVar(&opts.injectAppTemplate, "inject-app-template", EnvBoolOrDef("INJECT_APP_TEMPLATE", false), "inject app-template to structure (useful for app-template repository)")
	flags.StringSliceVarP(&opts.ValueFiles, "values", "f", []string{}, "specify values in a YAML file or a URL (can specify multiple)")
	flags.StringArrayVar(&opts.Values, "set", []string{}, "set values on the command line (can specify multiple or separate values with commas: key1=val1,key2=val2)")
	flags.StringArrayVar(&opts.StringValues, "set-string", []string{}, "set STRING values on the command line (can specify multiple or separate values with commas: key1=val1,key2=val2)")
	flags.StringArrayVar(&opts.FileValues, "set-file", []string{}, "set values from respective files specified via the command line (can specify multiple or separate values with commas: key1=path1,key2=path2)")
	flags.BoolVar(&opts.lintAllValueFiles, "all", false, "check all files in helm directory (flag --values will be rewritten)")

	return cmd
}

func runAll(args []string, opts *options) error {
	var failed bool
	workDir := WorkDir(args)
	for _, filename := range commonUsedValueFiles {
		_, err := os.Stat(path.Join(workDir, filename))
		if err != nil {
			continue
		}
		opts.ValueFiles = []string{
			path.Join(workDir, filename),
		}
		err = run(args, opts)
		if err != nil {
			logrus.Error(err)
			failed = true
		}
	}
	if failed {
		return errors.New("failed")
	}
	return nil
}

func run(args []string, opts *options) error {
	var (
		valid = true
	)

	workDir := WorkDir(args)
	commonValuesDir := ExtractCommonDir(opts.ValueFiles)
	if commonValuesDir != "" {
		workDir = commonValuesDir
	}

	opts.ValueFiles = append([]string{
		path.Join(workDir, "values.yaml"),
	}, opts.ValueFiles...)

	logrus.Infof("Files: %s", strings.Join(opts.ValueFiles, ", "))

	if len(opts.Values) > 0 {
		logrus.Infof("Values: %s", strings.Join(opts.Values, ", "))
	}

	if len(opts.StringValues) > 0 {
		logrus.Infof("String Values: %s", strings.Join(opts.StringValues, ", "))
	}

	if len(opts.FileValues) > 0 {
		logrus.Infof("File Values: %s", strings.Join(opts.FileValues, ", "))
	}

	vals, err := opts.MergeValues(nil)
	if err != nil {
		return err
	}

	if opts.injectAppTemplate {
		modifiedVals := make(map[string]interface{})
		modifiedVals["app-template"] = vals
		vals = modifiedVals
	}

	schemaVersion := ExtractSchemaVersion(vals)
	schemaTypes := ExtractTypes(opts.ValueFiles)

	for _, schemaType := range schemaTypes {
		schema, err := LoadSchema(schemaType, schemaVersion)
		if err != nil {
			if schemaType == "all" {
				return err
			}
			continue
		}
		result, err := ValidateWithSchema(schema, vals)
		if err != nil {
			return err
		}
		for _, err := range result.Errors() {
			valid = false
			logrus.Error(err)
		}
	}

	if !valid {
		return errors.New("validation failed")
	}

	logrus.Info("Looks good")
	return nil
}
