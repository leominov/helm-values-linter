package main

import "time"

type DurationFormat struct{}

func (d DurationFormat) IsFormat(dur interface{}) bool {
	durString, ok := dur.(string)
	if !ok {
		return false
	}
	_, err := time.ParseDuration(durString)
	return err == nil
}
