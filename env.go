package main

import (
	"os"
	"strconv"
)

func EnvBoolOrDef(name string, def bool) bool {
	if v, ok := os.LookupEnv(name); ok {
		if b, err := strconv.ParseBool(v); err == nil {
			return b
		}
	}
	return def
}
