package main

import (
	"os"

	"github.com/sirupsen/logrus"
)

var exitFn = os.Exit

func init() {
	logrus.SetOutput(os.Stdout)
	logrus.SetFormatter(&logrus.TextFormatter{
		ForceColors: true,
	})
}

func main() {
	cmd := newCommand()
	err := cmd.Execute()
	if err != nil {
		exitFn(1)
	}
}
