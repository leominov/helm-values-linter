package main

import "path"

func WorkDir(args []string) string {
	dir := "."
	if len(args) > 0 {
		dir = args[0]
	}
	return dir
}

func ExtractCommonDir(files []string) string {
	var dir string
	for _, f := range files {
		if dir == "" {
			dir = path.Dir(f)
			continue
		}
		if path.Dir(f) != dir {
			return ""
		}
	}
	return dir
}
