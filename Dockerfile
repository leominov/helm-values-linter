FROM golang:1.21-alpine3.17 as build
WORKDIR /go/src/gitlab.qleanlabs.ru/platform/infra/helm-values-linter
COPY . ./
RUN CGO_ENABLED=0 go build .

FROM alpine:3.17 as release
COPY --from=build /go/src/gitlab.qleanlabs.ru/platform/infra/helm-values-linter/helm-values-linter /usr/local/bin/helm-values-linter
