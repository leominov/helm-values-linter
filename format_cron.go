package main

import "github.com/gorhill/cronexpr"

type CronFormat struct{}

func (c CronFormat) IsFormat(cron interface{}) bool {
	cronString, ok := cron.(string)
	if !ok {
		return false
	}
	_, err := cronexpr.Parse(cronString)
	return err == nil
}
